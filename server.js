var express = require('express'),
    log = require('./utils/log.js'),
    app = express();
// Enviromet validation
if (app.get('env') != "production" && app.get('env') != "development") {
    log.error.app('You can only use environments : development or production');
    app.get('env') = 'development';
}
var path = require('path'),
    fs = require('fs'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    errorHandler = require('./utils/errorHandler.js'),
    dbObj = require('./utils/database');
var schedule = require('node-schedule');
var routes = require('./routes/index'),
    configuration = require('./config/config.json');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));
// Time start request
app.use(function (req, res, next) {
    req._responseTime = new Date().getTime();
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// PUSH Notification
//require('./utils/pushNotification-ios.js').init();
//require('./utils/pushNotification-android.js').init();
// EMAIL
//require('./utils/mail.js').init();
//require('./utils/mail.js').send();

// Bucket Amazon S3
//require('./utils/bucket.js').init();

//Facebook search
//require('./utils/facebook.js').search();

//Twitter search
require('./utils/twitter.js').init();

//Instagram search
//require('./utils/instagram.js').init();
//require('./utils/instagram.js').searchPromesis();

//Youtube search
//require('./utils/youtube.js').init();
//require('./utils/youtube.js').searchPromesis();

var rule = new schedule.RecurrenceRule();
rule.minute = new Date().getMinutes();

var j = schedule.scheduleJob(rule, function () {
    log.success.app("Job scheduleted");
    //    require('./utils/facebook.js').searchPromesis();
    require('./utils/twitter.js').searchPromesis();
    //    require('./utils/instagram.js').searchPromesis();
    //    require('./utils/youtube.js').searchPromesis();
});


// WELCOME API
/* GET home page. */
//app.get('/', function (req, res) {
//    res.render('index', {
//        title: 'Cactus mode ' + app.get('env'),
//        ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
//    });
//});
app.get('/hello', function (req, res) {
    res.status(200).json({
        message: " (っ◕‿◕)っ <( nice to meet you)"
    })
});
// APIs
app.use('/api/v1/', routes);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    return next({
        httpCode: 404,
        code: 103,
        message: req.url
    });
});

// error handler
app.use(errorHandler(configuration.appName));
app.set('views', __dirname + '/client/views');
app.use(express.static(path.join(__dirname, 'public')));
app.get('/', function (req, res, next) {
    res.redirect("index.html");
});
// Setting port and ip
app.set('port', process.env.OPENSHIFT_NODEJS_PORT  ? process.env.OPENSHIFT_NODEJS_PORT  : 'localhost');
app.set('ip', process.env.OPENSHIFT_NODEJS_IP ? process.env.OPENSHIFT_NODEJS_IP : 8000);

// Connection
if (configuration.connection[app.get('env')].protocol == "HTTP") {
    log.success.app(configuration.connection[app.get('env')].protocol);
    log.success.app("Cpus : " + require('os').cpus().length);
    log.success.app("Env : " + app.get('env'));
    app.listen(app.get('port'), app.get('ip'), function () {
        log.success.app('Server started on: ' + app.get('ip') + ', at port: ' + app.get('port'));
    });

} else {

    var https = require('https');
    // HTTPS certificate
    try {
        var _hskey = fs.readFileSync(configuration['HTTPS'][app.get('env')].key),
            _hscert = fs.readFileSync(configuration['HTTPS'][app.get('env')].cert);
    } catch (e) {
        return log.error.app('Missing HTTPS certificates');
    }
    var _options = {
        key: _hskey,
        cert: _hscert
    };
    // Start HTTPS server
    https.createServer(_options, app).listen(app.get('port'), app.get('ip'), function () {
        log.success.app(configuration.connection[app.get('env')].protocol);
        log.success.app("Cpus : " + require('os').cpus().length);
        log.success.app("Env : " + app.get('env'));
        log.success.app('Server started on: ' + app.get('ip') + ', at port: ' + app.get('port'));
    });
}

module.exports = app;
