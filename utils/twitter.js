var Twitter = require('node-twitter'),
    app = require('express')(),
    log = require('../utils/log.js'),
    Q = require('q'),
    configuration = require("../config/config.json");
var db = require('./database').db;
var twitterSearchClient;
var arrayProm = [];

function _init() {
    twitterSearchClient = new Twitter.SearchClient(
        configuration.TWITTER[app.get('env')].Consumer_Key,
        configuration.TWITTER[app.get('env')].Consumer_Secret,
        configuration.TWITTER[app.get('env')].Access_Token,
        configuration.TWITTER[app.get('env')].Access_Token_Secret
    );
    _searchAndScrol();
}

function _search() {
    twitterSearchClient.search({
        'q': configuration.hashtag
    }, function (error, result) {
        if (error) {
            log.error.api('[Twitter] Error: ' + (error.code ? error.code + ' ' + error.message : error.message));
            return;
        }
        if (result) {
            log.success.api("[Twitter] data: " + result.statuses.length);
            for (var a in result.statuses) {
                _saveInDb(result.statuses[a]);
            }
        }
    });
};


function _searchAndScrol(prev) {
    if (!prev) {
        twitterSearchClient.search({
            'q': configuration.hashtag
        }, function (error, result) {
            if (error) {
                log.error.api('[Twitter] Error: ' + (error.code ? error.code + ' ' + error.message : error.message));
                return;
            }
            if (result) {
                log.success.api("[Twitter] data: " + result.statuses.length);
                Q().then(function () {
                    var arrayProm = [];
                    for (var i = 0; i < result.statuses.length; i++) {
                        arrayProm.push(_saveInDbSincred(result.statuses[i]));
                    }
                    return arrayProm;
                }).all().then(function (promises) {
                    var conti = true;
                    for (var n in promises) {
                        if (promises[n].continue == false) {
                            conti = false
                        }
                    }
                    if (result.search_metadata.next_results) {
                        var id = result.search_metadata.next_results;
                        id = id.split("=");
                        id = id[1];
                        id = id.substr(0, id.length - 2);
                        if (id) {
                            _searchAndScrol(id);
                        } else {
                            log.success.api("[Twitter] database updated ");
                            return;
                        }
                    } else {
                        log.success.api("[Twitter] database updated ");
                        return;
                    }
                });
            }
        });

    } else {
        twitterSearchClient.search({
            'q': configuration.hashtag,
            'max_id': prev,
            'include_entities': 1
        }, function (error, result) {
            if (error) {
                log.error.api('[Twitter] Error: ' + (error.code ? error.code + ' ' + error.message : error.message));
                return;
            }
            if (result) {
                log.success.api("[Twitter] data: " + result.statuses.length);
                Q().then(function () {
                    var arrayProm = [];
                    for (var i = 0; i < result.statuses.length; i++) {
                        arrayProm.push(_saveInDbSincred(result.statuses[i]));
                    }
                    return arrayProm;
                }).all().then(function (promises) {
                    var conti = true;
                    for (var n in promises) {
                        if (promises[n].continue == false) {
                            conti = false
                        }
                    }
                    if (result.search_metadata.next_results) {
                        var id = result.search_metadata.next_results;
                        id = id.split("=");
                        id = id[1];
                        id = id.substr(0, id.length - 2);
                        if (id) {
                            _searchAndScrol(id);
                        } else {
                            log.success.api("[Twitter] database updated ");
                            return;
                        }
                    } else {
                        log.success.api("[Twitter] database updated ");
                        return;
                    }
                });
            }
        });
    }
};


function _saveInDb(us) {
    db.user.find({
        socialID: us.id
    }).exec(function (err, var_db) {
        if (err) {
            return;
        }
        if (var_db.length > 0) {
            log.error.database("[Twitter] just exist");
            return;
        } else {
            var User_to_push = new db.user({
                date: new Date(),
                update: new Date(),
                socialID: us.id,
                publishedAt: new Date(us.created_at),
                UserShort: {
                    image: us.user.profile_image_url,
                    name: us.user.screen_name,
                    socialID: us.user.id
                },
                image: (us.entities.media) ? us.entities.media[0].media_url : "",
                postMessage: us.text,
                socials: {
                    provider: "twitter",
                    user: us.user
                },
                otherSocialInfo: {
                    entities: us.entities,
                    retweet_count: us.retweet_count,
                    favorite_count: us.favorite_count
                }
            });
            User_to_push.save(function (err) {
                if (err) log.error.database("[Twitter] err: " + err);
                else log.success.database("[Twitter] saved: " + User_to_push.socialID);
            });
        }
    });
}


function _saveInDbSincred(us) {
    var deferred = Q.defer();
    db.user.find({
        socialID: us.user.id
    }).exec(function (err, var_db) {
        if (err) {
            return;
        }
        if (var_db.length > 0) {
            log.error.database("[Twitter] just exist");
            deferred.resolve({
                continue: false
            });
        } else {
            var User = new db.user({
                date: new Date(),
                update: new Date(),
                socialID: us.user.id,
                UserShort: {
                    image: us.user.profile_image_url,
                    name: us.user.screen_name,
                    socialID: us.user.id
                },
                image: us.user.profile_image_url
            });
            User.save(function (err) {
                if (err) {
                    log.error.database("[Twitter] err: " + err);
                    return deferred.reject();
                } else {
                    log.success.database("[Twitter] saved: " + User.socialID);
                    deferred.resolve({
                        continue: true
                    });
                }
            });

        }
    });
    return deferred.promise;
}

exports.searchPromesis = _searchAndScrol;
exports.search = _search;
exports.init = _init;
