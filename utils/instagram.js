var ig = require('instagram-node').instagram(),
    app = require('express')(),
    log = require('../utils/log.js'),
    configuration = require("../config/config.json");
var db = require('./database').db,
    Q = require('q');

var arrayProm = [];

function _init() {
    ig.use(configuration.INSTAGRAM[app.get('env')]);
}

function _search() {
    ig.tag_media_recent(configuration.tag, [], function (err, medias, pagination, remaining, limit) {
        if (err) {
            log.error.api("[Instagram] err: " + err);
        } else {
            log.success.api("[Instagram] data: " + medias.length);
            for (var a in medias) {
                _saveInDb(medias[a]);
            }
        }
    });
}

function _searchAndScrol(prev) {
    if (!prev) {
        ig.tag_media_recent(configuration.tag, [], function (err, medias, pagination, remaining, limit) {
            if (err) {
                log.error.api("[Instagram] err: " + err);
            } else {
                log.success.api("[Instagram] data: " + medias.length);
                Q().then(function () {
                    var arrayProm = [];
                    for (var i = 0; i < medias.length; i++) {
                        arrayProm.push(_saveInDbSincred(medias[i]));
                    }
                    return arrayProm;
                }).all().then(function (promises) {
                    var conti = true;
                    for (var n in promises) {
                        if (promises[n].continue == false) {
                            conti = false
                        }
                    }
                    if (pagination.next_max_tag_id) {
                        _searchAndScrol({
                            maxtag: pagination.next_max_tag_id,
                            mintag: pagination.min_tag_id
                        });
                    } else {
                        log.success.api("[Instagram] database updated ");
                    }
                });
            }
        });
    } else {
        ig.tag_media_recent(configuration.tag, [prev.mintag, prev.maxtag], function (err, medias, pagination, remaining, limit) {
            if (err) {
                log.error.api("[Instagram] err: " + err);
            } else {
                log.success.api("[Instagram] data: " + medias.length);
                Q().then(function () {
                    var arrayProm = [];
                    for (var i = 0; i < medias.length; i++) {
                        arrayProm.push(_saveInDbSincred(medias[i]));
                    }
                    return arrayProm;
                }).all().then(function (promises) {
                    var conti = true;
                    for (var n in promises) {
                        if (promises[n].continue == false) {
                            conti = false
                        }
                    }
                    if (pagination.next_max_tag_id) {
                        _searchAndScrol({
                            maxtag: pagination.next_max_tag_id,
                            mintag: pagination.min_tag_id
                        });
                    } else {
                        log.success.api("[Instagram] database updated ");
                    }
                });
            }
        });
    }

}

function _saveInDb(us) {
    db.post.find({
        socialID: us.id
    }).exec(function (err, var_db) {
        if (err) {
            return;
        }
        if (var_db.length > 0) {
            log.error.database("[Instagram] just exist");
            return;
        } else {
            var Post = new db.post({
                date: new Date(),
                update: new Date(),
                socialID: us.id,
                publishedAt: new Date(us.created_time * 1000),
                UserShort: {
                    image: us.user.profile_picture,
                    name: us.user.username,
                    socialID: us.user.id
                },
                image: us.images.standard_resolution.url,
                postMessage: (us.caption != undefined) ? us.caption.text : "",
                socials: {
                    provider: "instagram",
                    user: us.user
                },
                otherSocialInfo: {
                    link: us.link,
                    tags: us.tags,
                    users_in_photo: us.users_in_photo
                }
            });
            Post.save(function (err) {
                if (err) log.error.database("[Instagram] err: " + err);
                else log.success.database("[Instagram] saved: " + Post.socialID);
            });
        }
    });
}

function _saveInDbSincred(us) {
    var deferred = Q.defer();
    db.post.find({
        socialID: us.id
    }).exec(function (err, var_db) {
        if (err) {
            return;
        }
        if (var_db.length > 0) {
            log.error.database("[Instagram] just exist");
            deferred.resolve({
                continue: false
            });
        } else {
            var Post = new db.post({
                date: new Date(),
                update: new Date(),
                socialID: us.id,
                publishedAt: new Date(us.created_time * 1000),
                UserShort: {
                    image: us.user.profile_picture,
                    name: us.user.username,
                    socialID: us.user.id
                },
                image: us.images.standard_resolution.url,
                postMessage: (us.caption != undefined) ? us.caption.text : "",
                socials: {
                    provider: "instagram",
                    user: us.user
                },
                otherSocialInfo: {
                    link: us.link,
                    tags: us.tags,
                    users_in_photo: us.users_in_photo
                }
            });
            Post.save(function (err) {
                if (err) {
                    log.error.database("[Instagram] err: " + err);
                    return deferred.reject();
                } else {
                    log.success.database("[Instagram] saved: " + Post.socialID);
                    deferred.resolve({
                        continue: true
                    });
                }
            });
        }
    });
    return deferred.promise;
}

exports.searchPromesis = _searchAndScrol;
exports.init = _init;
exports.search = _search;
