// Database Manager
var mongoose = require('mongoose'),
    models = require('../models'),
    configuration = require('../config/config.json'),
    log = require('../utils/log.js');
var app = require('express')();

var dbUrl = configuration.db[app.get('env')].url;
try {
    var database = mongoose.createConnection(dbUrl);
} catch (e) {
    return log.error.app('Missing database connection url');
}

database.on('error', function (err) {
    log.error.database("Connection error: " + dbUrl + "\n" + err);
});
database.on('open', function () {
    log.success.database("Connection correct " + dbUrl + "\n");
});

var dbInstances = {
    post: database.model('Post', models.Post, 'post'),
    user: database.model('User', models.User, 'user'),
};

// Db
exports.db = dbInstances;
// Database Middelware
exports.middleware = function _db(req, res, next) {
    req.db = dbInstances;
    return next();
};
