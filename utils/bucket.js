//  next() --> req.picture
var log = require('../utils/log.js'),
    AWS = require('aws-sdk'),
    fs = require('fs'),
    Q = require('q'),
    ObjectID = require('mongodb').ObjectID,
    configuration = require('../config/config.json'),
    app = require('express')();

var _s3;

function _init() {
    //Amazon configuration
    if (!configuration.AWS.secretAccessKey || !configuration.AWS.secretAccessKey) return log.error.app('Missing AWS conf');

    AWS.config.update(configuration.AWS[app.get('env')]);
    _s3 = new AWS.S3({
        endpoint: configuration.AWS[app.get('env')].endpoint ? configuration.AWS[app.get('env')].endpoint : undefined
    });
    _s3.listBuckets(function (error, data) {
        if (error) {
            log.error.app("Bucket Connection: " + configuration.AWS[app.get('env')].accessKeyId);
            log.error.app("Buckets: " + JSON.stringify(data));
        }
        return log.success.app("Bucket Connection: " + JSON.stringify(error));
    });
}
exports.init = _init;

/* obj  RETURN PROMISES
{
    imageName : STRING.png,
    image : STRING [base64 valid],  required
    ACL : STRING
}
*/
function uploadOne(obj) {
    return Q.Promise(function (resolve, reject, notify) {
        var data = new Buffer(obj.image, 'base64');
        var _photoName = obj.imageName ? imageName : new ObjectID() + ".png";

        // File configuration
        var params = {
            Bucket: configuration.BUCKET[app.get('env')].name,
            Key: configuration.BUCKET[app.get('env')].profileFolderName + '/' + _photoName,
            ACL: obj.ACL ? obj.ACL : 'public-read',
            Body: data
        };

        var _photoURL = "http://" + configuration.BUCKET[app.get('env')].path +
            configuration.BUCKET[app.get('env')].name + "/" +
            configuration.BUCKET[app.get('env')].profileFolderName + '/' +
            _photoName;

        log.info.api("[Bucket] - Registration - Upload photo", {
            FileParams: {
                Bucket: configuration.BUCKET[app.get('env')].name,
                Key: configuration.BUCKET[app.get('env')].profileFolderName + '/' + _photoName,
                ACL: obj.ACL ? obj.ACL : 'public-read'
            }
        });
        _s3.putObject(params, function (error, data) {
            if (error) {
                log.error.api("[Bucket] - Registration - Upload photo", error);
                // Error
                return reject(error);
            } else {
                // success To Upload
                log.success.api("[Bucket] - Registration - Upload photo", {
                    PhotoUrl: _photoURL
                });
                var _obj = {};
                _obj.url = _photoURL;
                _obj.photoName = _photoName;
                return resolve(_obj);
            }
        });

        function onprogress(event) {
            notify(event.loaded / event.total);
        }
    });
};

/* obj  RETURN PROMISES
{
    imageName : STRING.png,
    images : [STRINGS] [base64 valid],  required
    ACL : STRING
}
*/
function uploadMore(obj) {
    var imgs = obj.images;
    return (function prom() {
        var array = [];
        for (var i in imgs) {
            array.push(uploadRequestFunction(imgs[i], i));
        }
        try {
            return Q.all(array);
        } catch (e) {
            log.error.api("[Bucket] - Registration - Upload photo", e);
            // Error
            return next({
                code: 100,
                message: 'Upload error',
                httpCode: 500
            });
        }
    })(); // Promises
};

// Upload one photo on Amazon
function uploadRequestFunction(image, index, name, ACL) {
    var deferred = Q.defer(),
        start = (new Date).getTime();
    //  console.log(index);
    try {
        var data = new Buffer(image, 'base64');
        var _photoName = name ? name + "-" + index + ".png" : new ObjectID() + "-" + index + ".png";
        // File configuration
        var params = {
            Bucket: configuration.BUCKET[app.get('env')].name,
            Key: configuration.BUCKET[app.get('env')].profileFolderName + '/' + _photoName,
            ACL: ACL ? ACL : 'public-read',
            Body: data
        };
        log.info.api("[Bucket] - Registration - Upload photo", {
            FileParams: {
                Bucket: configuration.BUCKET[app.get('env')].name,
                Key: configuration.BUCKET[app.get('env')].profileFolderName + '/' + _photoName,
                ACL: ACL ? ACL : 'public-read'
            }
        });

        _s3.putObject(params, function (error, data) {
            if (error) {
                log.error.api("[Bucket] - Registration - Upload photo", {
                    error: error
                });
                // console.log("ERROR" + JSON.stringify(error))
                return deferred.reject();
            } else {
                // success To Upload
                log.success.api("[Bucket] - Registration - Upload photo", {
                    PhotoUrl: _photoURL
                });
                var _photoURL = "http://" + configuration.BUCKET[app.get('env')].path +
                    configuration.BUCKET[app.get('env')].name + "/" +
                    configuration.BUCKET[app.get('env')].profileFolderName + '/' +
                    _photoName;
                return deferred.resolve(_photoURL);
            }
        });

    } catch (e) {
        return deferred.reject();
    }
    return deferred.promise;
};


// Exports  Rerurns promises
exports.uploads = {
    iamgeBase64: uploadMore
}

exports.upload = {
    iamgesBase64: uploadOne
}
