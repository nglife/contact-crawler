var log = require('../utils/log.js');
var conf = require('../config/config.json');
exports.getAll = function (req, res) {
    var page = req.param("page");
    var itemforpage = req.param("itemforpage");
    if (Number(page) && Number(itemforpage)) {
        page = (page < 1) ? 1 : page;
        var realpage = itemforpage * (page - 1);
        req.db.post.find({}).skip(Number(realpage)).limit(Number(itemforpage)).sort({
            'publishedAt': -1
        }).exec(function (err, posts) {
            if (err) {
                log.error.api("[wall] get all " + err);
                return res.status(500).json({
                    message: "sorry internal server error",
                    err: JSON.stringify(err)
                });
            } else {
                log.success.api("[wall] get all " + posts.length);
                page++;
                return res.status(200).json({
                    data: posts,
                    next: "/api/v1/wall/" + page + "/" + itemforpage
                });
            }
        })
    } else {
        req.db.post.find({}).limit(20).sort({
            'publishedAt': -1
        }).exec(function (err, posts) {
            if (err) {
                log.error.api("[wall] get all " + err);
                return res.status(500).json({
                    message: "sorry internal server error",
                    err: JSON.stringify(err)
                });
            } else {
                log.success.api("[wall] get all " + posts.length);
                return res.status(200).json({
                    data: posts,
                    next: "/api/v1/wall/2/20"
                });
            }
        })
    }
}

exports.validName = function (req, res, next) {
    var name = req.param("name");
    if (name == "locicero" || name == "cairoli" || name == "vinci" || name == "martinengo" || name == "gasperini") {
        next();
    } else {
        return res.status(400).json({
            message: "Bad Request"
        });
    }
};


exports.getAllPost = function (req, res) {
    var name = req.param("name");
    var page = req.param("page");
    var itemforpage = req.param("itemforpage");
    var realpage = itemforpage * (page - 1);
    var info = conf[name];
    if (Number(page) && Number(itemforpage)) {
        page = (page < 1) ? 1 : page;
        var realpage = itemforpage * (page - 1);
        req.db.post.find().or([{
            'UserShort.socialID': info.facebook
        }, {
            'UserShort.name': info.instagram
        }, {
            'UserShort.name': info.twitter
        }, {
            'UserShort.name': info.YouTube
    }]).skip(Number(realpage)).limit(Number(itemforpage)).sort({
            'publishedAt': -1
        }).exec(function (err, posts) {
            if (err) {
                log.error.api("[Ambassador: " + name + "] get all " + err);
                return res.status(500).json({
                    message: "sorry internal server error",
                    err: JSON.stringify(err)
                });
            } else {
                log.success.api("[Ambassador: " + name + "] get all " + posts.length);
                return res.status(200).json({
                    data: posts,
                    next: "/api/v1/ambassador/" + name + "/" + (++page) + "/" + itemforpage
                });
            }
        });
    } else {
        req.db.post.find().or([{
            'UserShort.socialID': info.facebook
    }, {
            'UserShort.name': info.instagram
    }, {
            'UserShort.name': info.twitter
    }, {
            'UserShort.name': info.YouTube
    }]).limit(20).sort({
            'publishedAt': -1
        }).exec(function (err, posts) {
            if (err) {
                log.error.api("[Ambassador: " + name + "] get all " + err);
                return res.status(500).json({
                    message: "sorry internal server error",
                    err: JSON.stringify(err)
                });
            } else {
                log.success.api("[Ambassador: " + name + "] get all " + posts.length);
                return res.status(200).json({
                    data: posts,
                    next: "/api/v1/ambassador/" + name + "/2/20"
                });
            }
        });
    }

};
