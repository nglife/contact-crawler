var express = require('express');
var router = express.Router(),
    _db = require('../utils/database').middleware,
    _post = require('./post');


router.get('/wall/:page/:itemforpage',
    _db,
    _post.getAll
);

router.get('/Ambassador/:name/:page/:itemforpage',
    _db,
    _post.validName,
    _post.getAllPost
);

module.exports = router;
