'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.view1',
  'myApp.view2',
  'myApp.version'
]).config(['$routeProvider', function ($routeProvider) {
    $routeProvider.otherwise({
        redirectTo: '/wall'
    });
}]).factory('Utils', function ($http) {
    return {
        getAll: function (success, error) {
            $http.get('/api/v1/wall/1/30').success(success).error(error);
        },
        getAllAmb: function (name, success, error) {
            $http.get('/api/v1/ambassador/' + name + '/1/30').success(success).error(error);
        },
        nextCall: function (nextCall, success, error) {
            $http.get(nextCall).success(success).error(error);
        }
    };
});
