'use strict';

angular.module('myApp.view2', ['ngRoute', 'ui.router', 'angularMoment', 'infinite-scroll', 'wu.masonry'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/ambassador/:nome', {
        templateUrl: 'view2/view2.html',
        controller: 'View2Ctrl'
    });
}]).controller('View2Ctrl', ['$routeParams', '$scope', 'Utils', function ($routeParams, $scope, Utils) {
    $scope.name = $routeParams.nome;
    $scope.nextCall;
    $scope.page = 1;
    if ($scope.name == "locicero" || $scope.name == "cairoli" || $scope.name == "vinci" || $scope.name == "martinengo" ||$scope.name =="gasperini" ) {
        $scope.loading = true;
        setTimeout(function () {
            $scope.loading = false;
            $scope.$digest();
        }, 2000);

        Utils.getAllAmb($scope.name, function (data) {
            $scope.items = data.data;
            $scope.nextCall = data.next;
            setTimeout(function () {
                for (var b in data.data) {
                    var c = linkHashtags(data.data[b].postMessage, data.data[b].socials.provider);
                    data.data[b].postMessage = c;
                    $(("#comment" + b + "")).html(c);
                    $scope.busy = false;
                }
            }, 100);
        }, function (err) {
            alert("err", err);
        });


        $scope.myPagingFunction = function () {
            if (!$scope.busy) {
                $scope.busy = true;
                Utils.nextCall($scope.nextCall, function (data) {
                    $scope.busy = true;
                    for (var n in data.data) {
                        $scope.items.push(data.data[n]);
                        $scope.busy = true;
                    }
                    setTimeout(function () {
                        for (var n in data.data) {
                            var c = linkHashtags(data.data[n].postMessage);
                            console.log(("#comment" + ((Number($scope.page) * 30) + Number(n)) + ""));
                            $(("#comment" + ((Number($scope.page) * 30) + Number(n)) + "")).html(c);
                            $scope.busy = true;
                        }
                        $scope.busy = false;
                        $scope.page++;
                    }, 10);
                    $scope.nextCall = data.next;
                }, function (err) {
                    console.log("err");
                    $scope.busy = false;
                });
            } else {
                console.log("cant make request");
                $scope.busy = true;
            }
        };
    } else {}


    function linkHashtags(text, provider) {
        var hashtag_regexp = /#([a-zA-Z0-9]+)/g;
        if (text) {
            if (provider == "twitter") {
                return text.replace(
                    hashtag_regexp,
                    '<a href="https://twitter.com/search?f=realtime&q=%23$1&src=typd" style="text-decoration: none;" target="_blanck"><span style="color:#17acdc;">#$1</span></a>'
                );
            }
            if (provider == "facebook") {
                return text.replace(
                    hashtag_regexp,
                    '<a href="https://www.facebook.com/hashtag/$1?fref=ts" style="text-decoration: none;" target="_blanck"><span style="color:#17acdc;">#$1</span></a>'
                );
            }
            if (provider == "youtube") {
                return text.replace(
                    hashtag_regexp,
                    '<a href="https://www.youtube.com/results?search_query=%23$1" style="text-decoration: none;" target="_blanck"><span style="color:#17acdc;">#$1</span></a>'
                );
            }
            if (provider == "instagram") {
                return text.replace(
                    hashtag_regexp,
                    '<a href="http://collec.to/tag/$1" style="text-decoration: none;" target="_blanck"><span style="color:#17acdc;">#$1</span></a>'
                );
            }
        } else {
            return
        }
    }
}]);
