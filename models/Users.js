// User model for database
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//External model for make dipendences
var User = new Schema({
    date: {
        type: Date,
        default: new Date()
    },
    update: {
        type: Date,
        default: new Date()
    },
    socialID: {
        type: String
    },
    UserShort: {
        image: {
            type: String
        },
        name: {
            type: String
        },
        socialID: {
            type: String
        }
    },
    image: {
        type: String
    }
});

exports.User = User;
